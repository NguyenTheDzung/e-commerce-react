import React, { useEffect, useState } from 'react';
import './home.css';
import Data from '../db.json';
import {uniqBy} from 'lodash';
import axios from 'axios';
import Content from '../content';
import {Link} from "react-router-dom"



function Home() {
    const [data, setData] = useState([])
    
    
   
useEffect(() => {
    const fetchData = async () => {
        const result = await axios.get('http://localhost:3004/products')
        setData(result.data)
    }
    fetchData()
}, [])

  return (

    <div className='text-content'>
        <div className='nav-left'>
            <section className='facet-wrapper'> 
            <h1 className='title-nav-left'>Show results for</h1>
            { data.length > 0 &&
            uniqBy(data, 'type').map((product) => {
                return (<div key={product.id} >
                    <Link to={`/${product.typeName}`} className='facet-item'>
                        
                        <span className='fact-name'> 
                        <i className="fa fa-angle-right"></i>{product.type}</span>
                    </Link>
                    </div>)
            })}
            
            </section>

            <section className='facet-wrapper'>
                <h1 className='title-nav-left'>Refine by</h1>
                <div className='facet-title'>Type</div>
                {
                uniqBy(data, 'detail-type').map((product) => (
                    <div key={product.id}>
                    <Link to={`/detail-type/${product['detail-type']}`} className='facet-item'>
                        <input type="checkbox" className=''value="Trend cases"></input>
                    <span className='fact-check'>{product['detail-type']}</span></Link>
                </div>
            ))}
                
                <div className='facet-title'>Brand</div>
                {
                uniqBy(data, 'brand').map((product) => (
               
                    <div>
                    <Link to={`/products-brand/${product.brand}`} className='facet-item'>
                        <input type="checkbox" className=''value="Trend cases"></input>
                    <span className='fact-check'>{product.brand}</span></Link>
                </div>
            ))}
                
                <div className='facet-title'>Ratings</div>

                <div className='star'>
                <div>
                    <a href="./" className='ais-star-rating--link' >
                        <span className='ratting'>
                        <i className='fa fa-star'style={{color:"#FF9900"}}></i></span>
                        <span className='ratting'>
                        <i className='fa fa-star'style={{color:"#FF9900"}}></i></span>
                        <span className='ratting'>
                        <i className='fa fa-star'style={{color:"#FF9900"}}></i></span>
                        <span className='ratting'>
                        <i className='fa fa-star'style={{color:"#FF9900"}}></i></span>
                        <span className='ratting'>
                        <i className='fa fa-star' style={{color:"#FFCCCC"}}></i></span>
                    </a>
                </div>
                <div>
                    <a href="./" className='ais-star-rating--link' >
                        <span className='ratting'>
                        <i className='fa fa-star'style={{color:"#FF9900"}}></i></span>
                        <span className='ratting'>
                        <i className='fa fa-star'style={{color:"#FF9900"}}></i></span>
                        <span className='ratting'>
                        <i className='fa fa-star'style={{color:"#FF9900"}}></i></span>
                        <span className='ratting'>
                        <i className='fa fa-star'style={{color:"#FFCCCC"}}></i></span>
                        <span className='ratting'>
                        <i className='fa fa-star' style={{color:"#FFCCCC"}}></i></span>
                    </a>
                </div>
                <div>
                        <a href="./" className='ais-star-rating--link' >
                        <span className='ratting'>
                        <i className='fa fa-star'style={{color:"#FF9900"}}></i></span>
                        <span className='ratting'>
                        <i className='fa fa-star'style={{color:"#FF9900"}}></i></span>
                        <span className='ratting'>
                        <i className='fa fa-star'style={{color:"#FFCCCC"}}></i></span>
                        <span className='ratting'>
                        <i className='fa fa-star'style={{color:"#FFCCCC"}}></i></span>
                        <span className='ratting'>
                        <i className='fa fa-star' style={{color:"#FFCCCC"}}></i></span>
                    </a>
                </div>
                <div>
                    <a href="./" className='ais-star-rating--link' >
                        <span className='ratting'>
                        <i className='fa fa-star' style={{color:"#FF9900"}}></i>
                        </span>
                        <span className='ratting'>
                        <i className='fa fa-star'style={{color:"#FFCCCC"}}></i></span>
                        <span className='ratting'>
                        <i className='fa fa-star'style={{color:"#FFCCCC"}}></i></span>
                        <span className='ratting'>
                        <i className='fa fa-star'style={{color:"#FFCCCC"}}></i></span>
                        <span className='ratting'>
                        <i className='fa fa-star' style={{color:"#FFCCCC"}}></i></span>
                    </a>
                </div>
                </div>
                <div className='prices'>
                    <div className='facet-title'>Prices</div>
                    <div>
                        <a className='facet-item' href=''>≤1</a>
                    </div>
                    <div>
                        <a className='facet-item' href=''>$1 - 80</a>
                    </div>
                    <div>
                        <a className='facet-item' href=''>$80 - 160</a>
                    </div>
                    <div>
                        <a className='facet-item' href=''>$160 - 240</a>
                    </div>
                    <div>
                        <a className='facet-item' href=''>$240 - 1,820</a>
                    </div>
                    <div>
                        <a className='facet-item' href=''>$1,820 - 3,400</a>
                    </div>
                    <div>
                        <a className='facet-item' href=''>$3,400 - 4,980</a>
                    </div>
                    <div>
                        <a className='facet-item' href=''>≥ $4,980</a>
                    </div>
                </div>
                <form className='form-prices'>
                    <label>
                        <span className='prices'>$</span>
                        <input type="number" className='input-prices' ></input>
                    </label>
                    <span>to</span>
                    <label>
                        <span className='prices'>$</span>
                        <input type="number" className='input-prices'></input>
                    </label>
                    <button className='btn-prices' type='submit'>GO</button>

                </form>
            </section>
        </div>
    </div>

    
  )
}
export default Home;
