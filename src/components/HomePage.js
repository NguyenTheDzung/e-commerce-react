import React from 'react';
import Content from '../content';
import Home from './home';

const HomePage = () => {
    return (
        <><Home />
        <Content />
        </>
    );
};

export default HomePage;