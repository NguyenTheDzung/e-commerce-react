import React, { useState, useEffect } from "react";
import axios from 'axios';
import './components/content.css';
import './components/nav.css';
import logo from './components/logo.jpg';
import './components/home.css';
import { Link, useParams } from "react-router-dom";


 
  

const renderStar = (rate) => {
    const splitRating = rate.slice(0,1);
    const listStar = [];
    for (let i=0; i < 5; i++) {
        if (i < splitRating) {
            listStar.push(<span className='ratting'>
            <i className='fa fa-star'style={{color:"#FF9900"}}></i></span>)
        }
        else {
            listStar.push(<span className='ratting'>
            <i className='fa fa-star' style={{color:"#FFCCCC"}}></i></span>)
        }
    }
    return listStar;
}

export default function Content() {
   
    const [currentPage, setCurrentPage] = useState(0);
    const [pageLimit] = useState(16);
    const [sortValue, setSortValue] = useState("");
    const params = useParams()
    const [sortFilterValue, setSortFilterValue] = useState("");
    const [operation, setOperation] = useState("");
   
    
    const [value, setValue] = useState("")

    
    const handleFilter = async() =>{
        let url
        if(params.detailType) {
            url = `http://localhost:3004/products?detail-type=${params.detailType}`
        } else if(params.name){
            url = `http://localhost:3004/products?typeName=${params.name}`
        }else{
            url = `http://localhost:3004/products?brand=${params.brand}`
        }
        return await axios
        .get(url)
        .then((response) => {
          setData(response.data);
        })
    }

    const handleSearch = async(e) => {
        loadProductData(0,16,0)
          return await axios.get(`http://localhost:3004/products?q=${e}`)
          .then((response) => {
            setData(response.data);
            setValue(value);
          })
          .catch((err) => console.log(err));
        }
    
    const [data, setData] = useState([]);


    

    const handleSortAsc = async(value) => {
        setSortValue(value);
        loadProductData(0,16,0, "sortasc");
        // return await axios
        // .get(`http://localhost:3004/products?_sort=price&_order=asc`)
        // .then((response) => {
        //    setData(response.data);
        // })
        // .catch((err) => console.log(err));
      }

      const handleSortDesc = async(value) => {
        
        setSortValue(value);
        loadProductData(0,16,0, "sortdesc", value);
      }

      const handleSort = async (e) => {
          const value = e.target.value
            if(value === "asc") {
                handleSortAsc(value)
            } else if (value === "desc") {
                handleSortDesc(value)}
                else{
                    renderPagination(loadProductData(0,16,0))
                }
      }
  

    useEffect(() => {
        if(!params.name) {
            loadProductData(0,16,0)
        } else {
            handleFilter()
            
        }
    }, [params.name]);

    useEffect(() => {
        if( params.detailType) {
            handleFilter()
        }
        
    }, [params.detailType]);

    useEffect(() => {
        if(params.brand) {
            handleFilter()
        }
    }, [params.brand]);


    const loadProductData = async (start, end, increase, optType=null, filterOrSortValue) => {
        switch (optType){
            case "sortasc":
                setOperation(optType);
                setSortFilterValue(filterOrSortValue);
                return await axios
                .get(`http://localhost:3004/products?_sort=price&_order=asc&_start=${start}&_end=${end}`)
                .then((response) => {
                   setData(response.data);
                   setCurrentPage(0)
                })
                .catch((err) => console.log(err));
            case "sortdesc":
                setOperation(optType);
                setSortFilterValue(filterOrSortValue);
                return await axios
                .get(`http://localhost:3004/products?_sort=price&_order=desc&_start=${start}&_end=${end}`)
                .then((response) => {
                   setData(response.data);
                   setCurrentPage(0);
                })
                .catch((err) => console.log(err));
                
            default:
                return await axios.get(`http://localhost:3004/products?_start=${start}&_end=${end}`)
                    .then((response) => {
                    setData(response.data);
                    setCurrentPage(currentPage + increase)
        })
        .catch((err) => console.log(err));

        }
        
    }

    const renderPagination = () =>{
        if (data.length < 16 && currentPage == 0 ) return null;
        if(currentPage === 0) {
            return (
                <nav aria-label="Page navigation example">
                    <ul class="pagination justify-content-center">
                    <li class="page-item"><a onClick={() => loadProductData(0,16,0)} class="page-link" href="#">1</a></li>
                    <li class="page-item">
                    <a onClick={() => loadProductData((currentPage + 1)*16,(currentPage + 2)*16,1,operation,sortFilterValue)} class="page-link" href="#">Next</a>
                    </li>
                    </ul>
                </nav>

            )
        }else if(currentPage < pageLimit -1 && data.length === pageLimit)  {
            return (
            <nav aria-label="Page navigation example">
            <ul class="pagination justify-content-center">
                <li class="page-item">
                    <a onClick={() => loadProductData((currentPage -1)*16, currentPage * 16, -1, operation,sortFilterValue)} class="page-link" href="#">Prev</a>
                </li>
                <li class="page-item"><a class="page-link" href="#">{currentPage + 1}</a></li>
                <li class="page-item">
                    <a onClick={() => loadProductData((currentPage + 1)*16,(currentPage + 2)*16,1,operation,sortFilterValue)} class="page-link" href="#">Next</a>
                </li>
                </ul>
            </nav>
            )
        }
        else{
            return (
            <nav aria-label="Page navigation example">
                <ul class="pagination justify-content-center">
                <li class="page-item">
                <a onClick={() => loadProductData((currentPage -1)*16, currentPage * 16, -1, operation,sortFilterValue)} class="page-link" href="#">Prev</a>
                </li>
                <li class="page-item"><a class="page-link" href="#">{currentPage + 1}</a></li>
                </ul>
            </nav>
            )
        }
    }
   

    const handleRest =() =>{
        currentPage(0);
        setCurrentPage(0);
        setOperation("");
        setValue("");
        setSortFilterValue("");
        setSortValue("");
        loadProductData(0,16,0)
    }
    
    return(
        
        
        <>
        <div className="sort-by">
            Sort by 
            <select onChange={handleSort} className="ais-sory-by-selector">
                <option className="ais-sort-by-selector--item" value="default">Featured</option>
                <option className="ais-sort-by-selector--item" value="asc">Price asc.</option>
                <option className="ais-sort-by-selector--item" value="desc">Price desc.</option>
                
            </select>
            <Link to="/" className="ais-sory-by-selector" onClick={handleRest}><button className="ais-sory-by-selector" >Reset</button></Link>
            
        </div>
              <div className='nav-header'>
                  <nav className="navbar navbar-light bg-dark">
                      <div className="container-fluid">
                          <form className="d-flex">
                          <img src={logo} alt="" width="65" height="45"></img>
                              <Link className="title" to="/">amazing</Link>
                              <input className="form-control me-0" type="search" 
                              placeholder="Search a product name"
                              onChange={(e) => handleSearch(e.target.value)} aria-label="Search"></input>
                              <button style={{width:"100px"}} type="button"  className="btn btn-warning">
                                <i className='fa fa-search'></i></button>
                          </form>
              </div>
          </nav>
              </div>
            )
        
        <div>
        <div className="content-product">
            <div>
                <div>
                    <div className="row">
                        {
                            data && data.length && data.map((product) => (
                                <div key={product.id} className='col-md-6 col-lg-3 hit' >
                                 <div className="hit-wrapper">
                                <div className='product-picture-wrapper'>
                                    <div className='product-picture'>
                                        <img src={product.image} />
                                    </div>
                                </div>
                                 <div className='product-desc-wrapper'>
                                    <div className='product-name'>{product.title}</div>
                                    {/* <div className="product-type">{product.type}</div> */}
                                    <div className="product-price">${product.price}</div>
                                    <div className='product-rating'>
                                        {renderStar(product.rating)}
                                    </div>
                                </div>
                                </div>
                             </div>
                            ))}
                        
                    </div>
            <div>{renderPagination()}</div>
                </div>
            </div>
        </div>
        </div>
        </>
    );
}
                        
