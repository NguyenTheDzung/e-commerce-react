
import { BrowserRouter, Route, Routes } from 'react-router-dom';
import Home from './components/home';
import HomePage from './components/HomePage';
import Content from './content';

function App() {
  return (
    <>
     <BrowserRouter>
      <Routes>
      <Route path='/' element={<HomePage />} />/detail-type/
      <Route path='/:name' element={<HomePage />} />
      <Route path='/detail-type/:detailType' element={<HomePage />} />
      <Route path='/products-brand/:brand' element={<HomePage />} />

      </Routes>
      </BrowserRouter>
    </>
  );
}

export default App;
